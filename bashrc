#!/bin/bash
#
# Configure environment for paludis
#

LC_ALL=en_US.UTF-8

CHOST="x86_64-pc-linux-gnu"

AM_OPTS="--ignore-deps"
# A positive number is taken as the maximum number of Perl threads
# to use in automake for generating multiple Makefile.in files
# concurrently.
AUTOMAKE_JOBS=8

# ATTENTION Need to fix some exheres ;-()
CMAKE_GENERATOR=Ninja
# Use colorful output if possible
CMAKE_COLOR_DIAGNOSTICS=ON
# Reduce verbosity enforced by arbor repo
CMAKE_NO_VERBOSE=1
#CMAKE_BUILD_PARALLEL_LEVEL=8
CMAKE_THEME=/home/exher/work/CMake/cmake/feat/cmake-theme/Themes/cmake-zaufi-theme.json

declare _n='\033[39m'
declare _w='\033[94m'
declare _f='\033[92m'
declare _r='\033[93m'
declare _u='\033[33m'
declare _p='\033[96m'
NINJA_STATUS="$(printf "${_w}%%es${_n}〉${_f}%%f${_n}/${_r}%%r${_n}/${_u}%%u${_n}〉${_p}%%p${_n}〉")"

# Enforce varous compliant tools to use color output
CLICOLOR=1
CLICOLOR_FORCE=1

#MISC_FLAGS="-fipa-pta -fweb"
SOME_O3_FLAGS="-fvect-cost-model=dynamic"
ARCH_FLAGS="-march=native" # -mtls-dialect=gnu2"
# Suppress useless^W warnings about unused local typedefs appeared for some packages
#CXXONLY_FLAGS="-Wno-unused-local-typedefs"
#CXXONLY_FLAGS="-fnothrow-opt -Wno-unused-local-typedefs"

_CFLAGS="-O2 -ggdb -pipe -fdiagnostics-color ${ARCH_FLAGS} ${SOME_O3_FLAGS} ${MISC_FLAGS}"
_CXXFLAGS="${_CFLAGS} ${CXXONLY_FLAGS}"
_LDFLAGS="-Wl,-O1 -Wl,--sort-common -Wl,--as-needed" #" -Wl,--enable-new-dtags -Wl,--hash-style=gnu"

x86_64_pc_linux_gnu_CFLAGS="${_CFLAGS}"
x86_64_pc_linux_gnu_CXXFLAGS="${_CXXFLAGS}"
x86_64_pc_linux_gnu_LDFLAGS="${_LDFLAGS}"

i686_pc_linux_gnu_CFLAGS="${_CFLAGS}"
i686_pc_linux_gnu_CXXFLAGS="${_CXXFLAGS}"
i686_pc_linux_gnu_LDFLAGS="${_LDFLAGS}"

# Suppress warnings from hooks
PALUDIS_FILESYSTEM_HOOK_NO_WARNING=yes

# Setup per package environment
#[[ -e /usr/libexec/paludis-hooks/setup_pkg_env.bash ]] && source /usr/libexec/paludis-hooks/setup_pkg_env.bash

# Detect terminal width dynamically for better [ ok ] align
save_COLUMNS=${COLUMNS}
COLUMNS=$(stty size 2>/dev/null | cut -d' ' -f2)
test -z "${COLUMNS}" && COLUMNS=${save_COLUMNS}
unset save_COLUMNS
PALUDIS_ENDCOL=$'\e[A\e['$(( ${COLUMNS:-80} - 7 ))'G'
